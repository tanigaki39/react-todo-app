import * as React from 'react'
import ReactDOM from 'react-dom'
import TodoList from './components/TodoList'
import TodoForm from './components/TodoForm'

const App = () => {
  const [todos, setTodos] = React.useState([])
  const addTodo = (text) => {
    setTodos([...todos, { id: Math.random().toString(32).substring(2), text: text }])
  }
  const deleteTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id))
  }

  React.useEffect(() => {
    setTodos(JSON.parse(localStorage.getItem('myTodos')) || [])
  }, [])

  React.useEffect(() => {
    localStorage.setItem('myTodos', JSON.stringify(todos))
  }, [todos])

  return (
    <div>
      <TodoList todos={todos} deleteTodo={deleteTodo} />
      <TodoForm handleClickAdd={addTodo} />
    </div>
  )
}

ReactDOM.render(<App />, document.querySelector('#app'))
