import * as React from 'react'
//____________________________________________
//
const Component = ({ todo: { text, id }, deleteTodo }) => {
  const handleClickDone = () => {
    deleteTodo(id)
  }

  return (
    <div>
      {text}
      <button onClick={handleClickDone}>完了</button>
    </div>
  )
}
//____________________________________________
//
export default Component
