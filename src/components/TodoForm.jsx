import * as React from 'react'
//____________________________________________
//
const Component = ({ handleClickAdd }) => {
  const [todo, setTodo] = React.useState('')
  const addTodo = () => {
    handleClickAdd(todo)
    setTodo('')
  }

  return (
    <div>
      <input onChange={(event) => setTodo(event.target.value)} value={todo} type="text" />
      <button onClick={addTodo}>追加</button>
    </div>
  )
}
//____________________________________________
//
export default Component
