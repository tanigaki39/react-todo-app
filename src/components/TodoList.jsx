import * as React from 'react'
import TodoItem from './TodoItem'
//____________________________________________
//
const Component = ({ todos, deleteTodo }) => (
  <ul>
    {todos.map((todo) => (
      <li key={todo.id}>
        <TodoItem todo={todo} deleteTodo={deleteTodo} />
      </li>
    ))}
  </ul>
)
//____________________________________________
//
export default Component
